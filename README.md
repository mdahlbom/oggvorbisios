# OggVorbisIOS #

This project wraps libogg-1.3.2 and libvorbis-1.3.4 into a Cocoa Touch (iOS) Framework. It contains everything required to encode/decode Ogg Vorbis audio in iOS applications.

## Licence ##

For the BSD-like licence under which libogg/libvorbis have been released, please see their licence page at [xiph.org.](http://www.xiph.org/licenses/bsd/) 

Any additional sources / configurations in this framework are available under the `Beer Licence`:

    The user is allowed to do anything with the licensed material. Should the user of the product meet 
    the author and consider the software useful, he is encouraged to buy the author a beer.

## HOWTO ##

Steps to build the framework:

1. Open the project file in Xcode
2. Select a simulator build (say, iPhone 6)
3. Edit the "OggVorbis" scheme and change the "Run" Build Configuration to `Release`
4. Hit `Product > Build` 
5. Your framework will be built into directory `~/Library/Developer/Xcode/DerivedData/OggVorbis-<something>/Build/Products/Release-iphonesimulator/OggVorbis.framework` 

Note that I haven't tested the encoding side myself so in case a header is missing, let me know.

## Code examples ##

Use **#import <OggVorbis/OggVorbis.h>** to include the headers into your project. 

See the following links for code examples.

### Decoding Ogg Vorbis into PCM audio: ###

* http://xiph.org/vorbis/doc/vorbisfile/example.html
* http://www.gamedev.net/page/resources/_/technical/game-programming/introduction-to-ogg-vorbis-r2031

### Encoding PCM audio into Ogg Vorbis: ###

* https://svn.xiph.org/trunk/vorbis/examples/encoder_example.c